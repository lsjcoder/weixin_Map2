var amapFile = require('../../libs/amap-wx.js');
var config = require('../../libs/config.js');

var markersData = [];
Page({
  data: {
    markers: [],
    latitude: '',
    longitude: '',
    textData: {},
    city: ''
  },
  makertap: function(e) {
    var id = e.markerId;
    var that = this;
    that.showMarkerInfo(markersData,id);
    that.changeMarkerColor(markersData,id);
  },
  onReady: function (e) {
    //关联<map />组件
    this.mapCtx = wx.createMapContext("map");
    this.movetoPosition();
  
  },
  onLoad: function(e) {

    // 1.设置地图控件的位置及大小，通过设备宽高定位
    wx.getSystemInfo({
      success: (res) => {
        this.setData({
          controls: [{
            id: 1,
            iconPath: '/img/location.png',
            position: {
              left: res.windowWidth - 50,
              top: res.windowHeight - 160,
              width: 50,
              height: 50
            },
            clickable: true
          },
          {
            id: 2,
            iconPath: '/img/marker-副本.png',
            position: {
              left: res.windowWidth / 2 - 11,
              top: res.windowHeight / 2 - 45,
              width: 22,
              height: 45
            },
            clickable: true
          }]
        })
      }
    });

    var that = this;
    var key = config.Config.key;
    var myAmapFun = new amapFile.AMapWX({key: key});
    var params = {
      iconPathSelected: '../../img/marker_checked.png',
      iconPath: '../../img/marker.png',
      success: function(data){
        markersData = data.markers;
        var poisData = data.poisData;
        var markers_new = [];
        markersData.forEach(function(item,index){
          markers_new.push({
            id: item.id,
            latitude: item.latitude,
            longitude: item.longitude,
            iconPath: item.iconPath,
            width: item.width,
            height: item.height
          })
        })
        if(markersData.length > 0){
          that.setData({
            markers: markers_new
          });
          that.setData({
            city: poisData[0].cityname || ''
          });
          that.setData({
            latitude: markersData[0].latitude
          });
          that.setData({
            longitude: markersData[0].longitude
          });
          that.showMarkerInfo(markersData,0);
        }else{
          wx.getLocation({
            type: 'gcj02',
            success: function(res) {
              that.setData({
                latitude: res.latitude
              });
              that.setData({
                longitude: res.longitude
              });
              that.setData({
                city: '北京市'
              });
            },
            fail: function(){
              that.setData({
                latitude: 39.909729
              });
              that.setData({
                longitude: 116.398419
              });
              that.setData({
                city: '北京市'
              });
            }
          })
          
          that.setData({
            textData: {
              name: '抱歉，未找到结果',
              desc: ''
            }
          });
        }
        
      },
      fail: function(info){
        // wx.showModal({title:info.errMsg})
      }
    }
    params.querykeywords = "足疗";
    myAmapFun.getPoiAround(params)
  },

  // 地图控件点击事件
  bindcontroltap: function (e) {
    // 判断点击的是哪个控件 e.controlId代表控件的id，在页面加载时的第2步设置的id
    switch (e.controlId) {
      // 点击定位控件
      case 1: this.movetoPosition();
        break;


      default: break;
    }
  },

  // 地图视野改变事件
  bindregionchange: function (e) {
    // 拖动地图，
    if (e.type == "begin") {

      // 停止拖动，
    } else if (e.type == "end") {
      var that = this
      this.mapCtx.getCenterLocation({
        success: function (res) {
          var key = config.Config.key;
          var myAmapFun = new amapFile.AMapWX({ key: key });
          var params = {
            iconPathSelected: '../../img/marker_checked.png',
            iconPath: '../../img/marker.png',
            success: function (data) {
              markersData = data.markers;
              var poisData = data.poisData;
              var markers_new = [];
              markersData.forEach(function (item, index) {
                markers_new.push({
                  id: item.id,
                  latitude: item.latitude,
                  longitude: item.longitude,
                  iconPath: item.iconPath,
                  width: item.width,
                  height: item.height
                })
              })
              if (markersData.length > 0) {
                that.setData({
                  markers: markers_new
                });
                that.showMarkerInfo(markersData, 0);
              }
            },
            fail: function (info) {
              // wx.showModal({title:info.errMsg})
              this.movetoPosition();
            }
          }
          params.querykeywords = "足疗";
          params.location = String(res.longitude) + ',' + String(res.latitude);
          myAmapFun.getPoiAround(params)
        }
      })

    }
  },

  showMarkerInfo: function(data,i){
    var that = this;
    that.setData({
      textData: {
        name: data[i].name,
        desc: data[i].address
      }
    });
  },
  changeMarkerColor: function(data,i){
    var that = this;
    var markers = [];
    for(var j = 0; j < data.length; j++){
      if(j==i){
        data[j].iconPath = "../../img/marker_checked.png";
      }else{
        data[j].iconPath = "../../img/marker.png";
      }
      markers.push({
        id: data[j].id,
        latitude: data[j].latitude,
        longitude: data[j].longitude,
        iconPath: data[j].iconPath,
        width: data[j].width,
        height: data[j].height
      })
    }
    that.setData({
      markers: markers
    });
  },
  // 定位函数，移动位置到地图中心
  movetoPosition: function () {
    this.mapCtx.moveToLocation();
  }

})